package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guod
 */
@SpringBootApplication
public class SsmPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsmPlusApplication.class, args);
    }
}
