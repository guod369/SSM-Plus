package cn.hacz.edu.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @author guod
 */
public class GeneratorServiceEntity {
    public static void main(String[] args) {
        /**
         *包名称
         */
        String packageName = "cn.hacz.edu.modules";
        /**
         *模块名称
         */
        String setModuleName = "sys";
        /**
         * user -> UserService, 设置成true: user -> IUserService
         */
        boolean serviceNameStartWithI = false;
        /**
         * 修改替换成你需要的表名，多个表名传数组
         */
        generateByTables(serviceNameStartWithI, packageName, setModuleName, "sys_user");
    }


    private static void generateByTables(boolean serviceNameStartWithI, String packageName, String setModuleName, String... tableNames) {
        GlobalConfig config = new GlobalConfig();
        String dbUrl = "jdbc:mysql://39.106.44.170:3306/ssm";
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setUsername("root")
                .setPassword("121528")
                .setDriverName("com.mysql.jdbc.Driver");
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
                .setTablePrefix(new String[]{"tb_", "sys_"})
                .setCapitalMode(true)
                .setEntityLombokModel(false)
                .setDbColumnUnderline(true)
                .setNaming(NamingStrategy.underline_to_camel)
                .setInclude(tableNames);
        config.setActiveRecord(false)
                .setAuthor("guod")
                .setOutputDir("D:\\2018dev\\code\\MpGenerator")
                .setBaseResultMap(true)
                .setFileOverride(true);
        if (!serviceNameStartWithI) {
            config.setServiceName("%sService");
        }
        new AutoGenerator().setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(
                        new PackageConfig()
                                .setParent(packageName)
                                .setModuleName(setModuleName)
                                .setController("controller")
                                .setEntity("model")
                ).execute();
    }

    private void generateByTables(String packageName, String setModuleName, String... tableNames) {
        generateByTables(true, packageName, setModuleName, tableNames);
    }

}
