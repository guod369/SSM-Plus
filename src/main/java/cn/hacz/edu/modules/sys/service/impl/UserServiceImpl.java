package cn.hacz.edu.modules.sys.service.impl;

import cn.hacz.edu.modules.sys.mapper.UserMapper;
import cn.hacz.edu.modules.sys.model.entity.User;
import cn.hacz.edu.modules.sys.service.UserService;
import cn.hacz.edu.webexception.MyException;
import cn.hacz.edu.webexception.ResultEnum;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author guod
 * @since 2018-02-05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public void getAge(Integer id) {
        User user = userMapper.selectById(id);
        Integer age = user.getAge();
        if (age < 10) {
            throw new MyException(ResultEnum.MIDDLE_SCHOOL);
        } else if (age > 10 && age < 16) {
            throw new MyException(ResultEnum.MIDDLE_SCHOOL);
        }
        System.out.println("成功");

    }

    @Override
    public void insertTwo() {
        User user01 = new User();
        user01.setName("g");
        user01.setAge(12);
        userMapper.insert(user01);

        User user02 = new User();
        user02.setAge(12);
        user02.setName("guod");
        userMapper.insert(user02);
    }
}
