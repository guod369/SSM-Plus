package cn.hacz.edu.modules.sys.controller;


import cn.hacz.edu.modules.sys.model.dto.UserDto;
import cn.hacz.edu.modules.sys.model.entity.User;
import cn.hacz.edu.modules.sys.service.UserService;
import cn.hacz.edu.webexception.R;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author guod
 * @since 2018-02-05
 */
@RestController
@Api(description = "用户测试接口", tags = "UserController")
public class UserController {
    private final static Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    /**
     * 功能：获取全部列表
     *
     * @return
     */
    @ApiOperation(value = "查找", notes = "根据用户ID查找用户")
    @GetMapping(value = "/api/userList")
    public R userList() {
        logger.info("获取用户列表");
        return R.ok();
    }

    /**
     * 功能：添加用户信息
     *
     * @param userDto
     * @param bindingResult
     * @return
     */
    @ApiOperation(value = "查找", notes = "添加用户信息")
    @PostMapping(value = "/api/userAdd")
    public R<User> userAdd(@Valid UserDto userDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getFieldError().getDefaultMessage());
            return R.error("-200", bindingResult.getFieldError().getDefaultMessage());
        }
        logger.info("添加用户");
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        userService.insert(user);
        return R.ok();
    }

    /**
     * 功能：添加用户信息
     *
     * @param user
     * @return
     */
    @ApiOperation(value = "添加用户", notes = "根据用户信息")
    @PostMapping("/api/addJsonUser")
    public R addJsonUser(@RequestBody @ApiParam(name = "用户对象", value = "传入JSON格式", required = true) User user) {
        return R.ok(userService.insert(user));
    }

    /**
     * 功能：删除用户
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除用户", notes = "根据ID删除用户")
    @DeleteMapping(value = "/api/deleteUser/{id}")
    public R deleteUser(@PathVariable("id") Integer id) {
        return R.ok(userService.deleteById(id));
    }

    /**
     * 功能：更新数据
     *
     * @param id
     * @param name
     * @param age
     * @return
     */
    @ApiOperation(value = "查找", notes = "根据用户ID查找用户")
    @PutMapping(value = "/api/updateUser/{id}")
    public R updateUser(@PathVariable("id") Integer id,
                        @RequestParam("name") String name,
                        @RequestParam("age") Integer age) {
        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setAge(age);
        return R.ok(userService.updateById(user));
    }

    /**
     * 功能：查询单个信息
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查找", notes = "根据用户ID查找用户")
    @GetMapping(value = "/api/findOneUser/{id}")
    public R findOneUser(@PathVariable("id") Integer id) {
        return R.ok(userService.selectById(id));
    }

    /**
     * 功能：查询所有
     *
     * @return
     */
    @ApiOperation(value = "查找", notes = "根据用户ID查找用户")
    @GetMapping(value = "/api/findManyUser")
    public R findManyUser() {
        return R.ok(userService.selectList(new EntityWrapper<>()));
    }

    /**
     * 功能：异常处理测试
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查找", notes = "根据用户ID查找用户")
    @GetMapping(value = "/api/getAgeUser/{id}")
    public R getAgeUser(@PathVariable("id") Integer id) {
        userService.getAge(id);
        return R.ok();
    }

    /**
     * 功能：事物测试
     *
     * @return
     */
    @ApiOperation(value = "查找", notes = "根据用户ID查找用户")
    @PostMapping(value = "/api/getTwoUser")
    public R getTwoUser() {
        userService.insertTwo();
        return R.ok();
    }

    /**
     * 功能：分页查询 10 条姓名为‘guod’的用户记录
     *
     * @return
     */
    @ApiOperation(value = "查找", notes = "根据用户ID查找用户")
    @GetMapping(value = "/api/getSelectPage")
    public R getSelectPage() {
        List<User> userList = userService.selectPage(
                new Page<>(1, 10),
                new EntityWrapper<User>().eq("name", "guod")
        ).getRecords();
        return R.ok(userList);
    }

}

