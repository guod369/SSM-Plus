package cn.hacz.edu.modules.sys.model.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * project -
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/2/5 时间:9:13
 * @JDK JDK1.8
 * @Description 功能模块：
 */
public class UserDto {

    private Integer id;
    @NotNull
    private String name;
    @NotNull
    @Max(25)
    @Min(18)
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
