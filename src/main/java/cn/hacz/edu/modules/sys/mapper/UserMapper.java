package cn.hacz.edu.modules.sys.mapper;

import cn.hacz.edu.modules.sys.model.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author guod
 * @since 2018-02-05
 */
@Component
public interface UserMapper extends BaseMapper<User> {

}
