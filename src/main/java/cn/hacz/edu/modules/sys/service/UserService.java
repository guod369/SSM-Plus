package cn.hacz.edu.modules.sys.service;

import cn.hacz.edu.modules.sys.model.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author guod
 * @since 2018-02-05
 */
public interface UserService extends IService<User> {

    void getAge(Integer id);

    void insertTwo();
}
