package cn.hacz.edu.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * project -
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/2/5 时间:8:37
 * @JDK JDK1.8
 * @Description 功能模块：
 */
@EnableTransactionManagement
@Configuration
@MapperScan("cn.hacz.edu.modules.sys.mapper")
public class MybatisPlusConfig {
    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
