package cn.hacz.edu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * project -
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/2/5 时间:11:00
 * @JDK JDK1.8
 * @Description 功能模块：
 */
@Configuration
@EnableSwagger2
public class Swagger2Configuration {
    @Bean
    public Docket accessToken() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("api")// 定义组
                .select() // 选择那些路径和api会生成document
                .apis(RequestHandlerSelectors.basePackage("cn.hacz.edu.modules.sys.controller")) // 拦截的包路径
                .paths(regex("/api/.*"))// 拦截的接口路径
                .build() // 创建
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()//
                .title("东东")// 标题
                .description("SSM-Plus入门")// 描述
                .termsOfServiceUrl("http://www.guod.top")//
                .contact(new Contact("guod", "http://www.guod.top", "gud369@163.com"))// 联系
                .license("Apache License Version 2.0")// 开源协议
                .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")// 地址
                .version("2.0")// 版本
                .build();
    }
}
